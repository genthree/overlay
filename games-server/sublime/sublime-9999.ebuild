# Copyright 1999-2017 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=6

inherit git-r3 user

DESCRIPTION="Minecraft server (and proxy server) with lore-friendly patches and additions from the sublime project"
HOMEPAGE="https://gitlab.com/minecraft-sublime"
SRC_URI=""

EGIT_REPO_URI_SPIGOT_BUILD_TOOLS="https://gitlab.com/minecraft-sublime/spigot-build-tools.git"
EGIT_REPO_URI_BUNGEECORD="https://github.com/SpigotMC/BungeeCord.git"
EGIT_REPO_URI_PLUGIN_PROTOCOLLIB="https://github.com/dmulloy2/ProtocolLib.git"
EGIT_REPO_URI_PLUGIN_DYNMAP="https://github.com/webbukkit/dynmap.git"
EGIT_REPO_URI_PLUGIN_WORLDBORDER="https://github.com/Brettflan/WorldBorder.git"
EGIT_REPO_URI_PLUGIN_WORLDEDIT="https://github.com/sk89q/WorldEdit.git"

if [[ $PV == 9999 ]] ; then
	KEYWORDS=""
else
	KEYWORDS="~amd64 ~x86"
	EGIT_COMMIT_DATE="${PV}"
fi

LICENSE_MINECRAFT="all-rights-reserved"
LICENSE_BUNGEECORD="BSD"
LICENSE_SPIGOT="MIT"
LICENSE_SUBLIME="MIT"
LICENSE_PLUGIN_PROTOCOLLIB="GPL-2"
LICENSE_PLUGIN_DYNMAP="Apache-2.0"
LICENSE_PLUGIN_WORLDBORDER="BSD"
LICENSE_PLUGIN_WORLDEDIT="GPL-3"

LICENSE="
	$LICENSE_MINECRAFT
	$LICENSE_BUNGEECORD
	$LICENSE_SPIGOT
	$LICENSE_SUBLIME
	$LICENSE_PLUGIN_PROTOCOLLIB
	plugin_dynmap?      ( $LICENSE_PLUGIN_DYNMAP      )
	plugin_worldborder? ( $LICENSE_PLUGIN_WORLDBORDER )
	plugin_worldedit?   ( $LICENSE_PLUGIN_WORLDEDIT   )
"
SLOT="0"
IUSE="
	+accept_eula
	+metrics_opt_out
	+bstats_opt_out
	+plugin_dynmap
	+plugin_worldborder
	+plugin_worldedit
"

RDEPEND="
	>=virtual/jre-1.8
	>=virtual/mysql-5.5
	app-misc/tmux
	>=sys-apps/process-utils-0.0.5
"
DEPEND="
	>=dev-java/maven-bin-3.3
	${RDEPEND}
"

src_unpack() {
	if has network-sandbox ${FEATURES}; then
		eerror "Cannot build with network-sandbox. Run with FEATURES=-network-sandbox"
	fi

	EGIT_REPO_URI=${EGIT_REPO_URI_SPIGOT_BUILD_TOOLS} EGIT_CHECKOUT_DIR=${WORKDIR}/${P} \
		git-r3_src_unpack

	EGIT_REPO_URI=${EGIT_REPO_URI_BUNGEECORD} EGIT_CHECKOUT_DIR=${WORKDIR}/${P}/bungeecord \
		git-r3_src_unpack

	EGIT_REPO_URI=${EGIT_REPO_URI_PLUGIN_PROTOCOLLIB} EGIT_CHECKOUT_DIR=${WORKDIR}/${P}/plugins/ProtocolLib \
		git-r3_src_unpack

	if use plugin_dynmap ; then
		EGIT_REPO_URI=${EGIT_REPO_URI_PLUGIN_DYNMAP} EGIT_BRANCH="v3.0" EGIT_CHECKOUT_DIR=${WORKDIR}/${P}/plugins/dynmap \
			git-r3_src_unpack
	fi

	if use plugin_worldborder ; then
		EGIT_REPO_URI=${EGIT_REPO_URI_PLUGIN_WORLDBORDER} EGIT_CHECKOUT_DIR=${WORKDIR}/${P}/plugins/WorldBorder \
			git-r3_src_unpack
	fi

	if use plugin_worldedit ; then
		EGIT_REPO_URI=${EGIT_REPO_URI_PLUGIN_WORLDEDIT} EGIT_CHECKOUT_DIR=${WORKDIR}/${P}/plugins/WorldEdit \
			git-r3_src_unpack
	fi
}

pkg_setup() {
	enewuser sublime
}

src_compile() {
	einfo "Compiling spigot"
	./build.sh \
		|| die "Could not compile 'spigot'"

	einfo "Compiling bungeecord"
	pushd bungeecord &> /dev/null \
		|| die "Could not change into 'bungeecord/'"
	mvn -B install \
		|| die "Could not compile 'bungecoord'"
	popd &> /dev/null

	einfo "Compiling plugin sublime"
	pushd sublime &> /dev/null \
		|| die "Could not change into 'sublime/'"
	mkdir libs \
		|| die "Could create create directory l'ibs/'"
	ln -s "../../spigot.jar" "libs/spigot.jar" \
		|| die "Could create link 'libs/spigot.jar'"
	mvn -B install \
		|| die "Could compile 'sublime'"
	popd &> /dev/null

	einfo "Compiling plugin ProtocolLib"
	pushd plugins/ProtocolLib &> /dev/null \
		|| die "Could not change into 'plugins/ProtocolLib/'"
	#chmod +x gradlew
	#./gradlew clean build shadowJar
	##mvn -B install \
	##	|| die "Could not compile 'ProtocolLib'"
	wget "http://ci.dmulloy2.net/job/ProtocolLib%20Gradle/lastSuccessfulBuild/artifact/build/libs/ProtocolLib.jar"
	popd &> /dev/null

	if use plugin_dynmap ; then
		einfo "Compiling plugin dynmap"
		pushd plugins/dynmap &> /dev/null \
			|| die "Could not change into 'plugins/dynmap/'"
		( mv "settings.gradle" "_settings.gradle" && grep -v forge < "_settings.gradle" > settings.gradle ) \
			|| die "Could not overwrite settings.gradle for building dynmap"
		./gradlew clean build install \
			|| die "Could not compile 'dynmap'"
		popd &> /dev/null
	fi

	if use plugin_worldborder ; then
		einfo "Compiling plugin WorldBorder"
		pushd plugins/WorldBorder &> /dev/null \
			|| die "Could not change into 'plugins/WorldBorder/'"
		mvn -B install \
			|| die "Could not compile 'WorldBorder'"
		popd &> /dev/null
	fi

	if use plugin_worldedit ; then
		einfo "Compiling plugin WorldEdit"
		pushd plugins/WorldEdit &> /dev/null \
			|| die "Could not change into 'plugins/WorldEdit/'"
		echo 'rootProject.name = "worldedit"
include("worldedit-libs")
include("worldedit-libs:bukkit")
include("worldedit-bukkit")
include("worldedit-libs:core")
include("worldedit-core")
include("worldedit-libs:core:ap")' > "settings.gradle" \
			|| die "Could not overwrite settings.gradle for building WorldEdit"
		./gradlew clean build \
			|| die "Could not compile 'WorldEdit'"
		popd &> /dev/null
	fi
}

src_install() {
	first() { echo "$1"; }
	newsed_() {
		local cmd="$1"
		local original="$2"
		local new="$3"
		shift 3

		cp "$original" _newsed_tmp
		sed -i "$@" _newsed_tmp
		"$cmd" _newsed_tmp "$new"
	}
	dosed_() {
		local cmd="$1"
		local file="$2"
		shift 2

		newsed_ "$cmd" "$file" "$(basename "$file")" "$@"
	}
	newsedins() { newsed_ newins "$@"; }
	dosedins() { dosed_ newins "$@"; }
	newsedconfd() { newsed_ newconfd "$@"; }
	dosedconfd() { dosed_ newconfd "$@"; }


	# Install server .jar files
	insinto /opt/sublime
	newins $(first "Spigot/Spigot-Server/target/spigot-"*"-SNAPSHOT.jar") spigot.jar

	insinto /opt/sublime-proxy
	newins "bungeecord/bootstrap/target/BungeeCord.jar" bungeecord.jar

	fperms 755 /opt/sublime{,-proxy}


	# Install plugins .jar files
	insinto /opt/sublime/plugins
	newins $(first "sublime/target/sublime-"*"-SNAPSHOT.jar") sublime.jar
	#newins "plugins/ProtocolLib/modules/ProtocolLib/target/ProtocolLib.jar" protocollib.jar
	newins "plugins/ProtocolLib/ProtocolLib.jar" protocollib.jar

	use plugin_dynmap \
		&& newins $(first "plugins/dynmap/target/Dynmap-"*"-spigot.jar") dynmap.jar
	use plugin_worldborder \
		&& newins "plugins/WorldBorder/target/WorldBorder.jar" worldborder.jar
	use plugin_worldedit \
		&& newins $(first "plugins/WorldEdit/worldedit-bukkit/build/libs/worldedit-bukkit-"*"-SNAPSHOT-dist.jar") worldedit.jar

	insinto /opt/sublime-proxy/plugins
	newins $(first "sublime/target/sublime-"*"-SNAPSHOT.jar") sublime.jar


	# Install helpers and shared files
	insinto /usr/share/sublime
	doins "${FILESDIR}"/share/{init,server}-helper
	doins sublime/sublime-local.conf

	# Install wrappers
	dosbin "${FILESDIR}"/bin/sublime{,-proxy}

	# Install init.d scripts
	newinitd "${FILESDIR}"/etc/sublime-init sublime
	newinitd "${FILESDIR}"/etc/sublime-proxy-init sublime-proxy

	# Install conf.d files
	newconfd "${FILESDIR}"/etc/sublime-conf sublime
	newconfd "${FILESDIR}"/etc/sublime-proxy-conf sublime-proxy


	# Copy configuration files
	keepdir /etc/sublime
	insinto /etc/sublime
	doins sublime/sublime.conf

	if use accept_eula ; then
		dosedins "${FILESDIR}"/server/eula.txt -e "s/^eula=false/eula=true/"
	else
		doins "${FILESDIR}"/server/eula.txt
	fi

	keepdir /etc/sublime-proxy
	insinto /etc/sublime-proxy
	newins sublime/sublime-proxy.conf sublime.conf

	insinto /etc/sublime/plugins
	doins -r "${FILESDIR}"/server/plugins/ProtocolLib
	use plugin_worldedit && doins -r "${FILESDIR}"/server/plugins/WorldEdit
	use metrics_opt_out && doins -r "${FILESDIR}"/server/plugins/PluginMetrics
	use bstats_opt_out && doins -r "${FILESDIR}"/server/plugins/bStats


	# Create system directories
	dodir /var/{,log/,lib/}sublime{,-proxy}
	dodir /var/lib/sublime{,-proxy}/{tmux,work,intermediate/{overlay,work}}
	dodir /var/lib/sublime/home

	# Set correct permissions

	# /etc/* directories should be r-x to sublime only, but not writable
	fowners root:sublime /etc/sublime{,-proxy}
	fperms 750 /etc/sublime{,-proxy}

	# /var/* should chmod 700 to root, /var/{log,lib}/{*,*/tmux} and should belong to sublime
	fowners sublime:sublime /var/log/sublime{,-proxy}
	fowners sublime:sublime /var/lib/sublime{,-proxy}/tmux
	fowners sublime:sublime /var/lib/sublime/home

	fperms -R 700 /var/{log/,lib/}sublime{,-proxy}
	fperms 755 /var/lib/sublime{,-proxy}
}

pkg_postinst() {
	if [[ $(egethome sublime) != /var/lib/sublime/home ]]; then
		esethome sublime /var/lib/sublime/home
	fi

	# TODO wiki entry, then move the text and hint there with ewarn.
	einfo "This package allows you to run an arbitrary number of servers, regardless"
	einfo "of the type of server (minecraft / bungeecord proxy). All of the"
	einfo "following information applies to both server types."
	einfo
	einfo "In order to create a server instance, you have to add a new init.d"
	einfo "file for it and adjust the server settings. The delimiting dot in"
	einfo "sublime.NAME is mandatory, apart from that the name may be chosen"
	einfo "freely. Nevertheless, avoid whitespace."
	einfo
	einfo "   # Create service"
	einfo "   cd /etc/init.d"
	einfo "   ln -s /usr/share/sublime/sublime sublime.NAME"
	einfo
	einfo "Optionally, the server can be prepared, by effectively starting"
	einfo "the server once and then stopping it immediately. This ensures,"
	einfo "that all main configuration files have been created."
	einfo
	einfo "   # Prepare files"
	einfo "   rc-service sublime.NAME prepare"
	einfo
	einfo "Now you should see, that the familiar minecraft server files"
	einfo "have been created in /var/sublime/NAME. Before you start editing"
	einfo "these, you should know that this package adds a new layer"
	einfo "of abstraction to the basic configuration model. In your system"
	einfo "you will now find three essential directories:"
	einfo
	einfo "/opt/sublime"
	einfo "   Contains the .jar files for the server and for plugins which"
	einfo "   were installed by this package. If you want to install a new plugin"
	einfo "   GLOBALLY (for all server instances), this is the directory"
	einfo "   where you want to put it."
	einfo
	einfo "/etc/sublime"
	einfo "   Contains GLOBAL server and plugin configuration files."
	einfo "   While technically possible, it is discouraged to put .jar files here."
	einfo "   Use /opt/sublime/plugins for that purpose."
	einfo
	einfo "/var/sublime/NAME"
	einfo "   This directory is the working directory of the sever instance NAME."
	einfo "   Plugins and configuration files specific to this instance belong here."
	einfo
	einfo "When any instance is started, both /etc/sublime and /opt/sublime will"
	einfo "be mounted \"below\" the instance's working directory using overlayfs."
	einfo "Likewise, the server specific directory can be seen as being \"on top\""
	einfo "of the others, which makes it possible to overwrite any configuration"
	einfo "file or delete a plugin just for one server instance. This saves a lot of"
	einfo "configuration work, while keeping all your global files read-only"
	einfo "to the ever so naughty minecraft server (ò_ó)"
	einfo
	einfo "With this in mind, you can proceed to make your configuration changes."
	einfo "Be aware, that /etc/sublime/sublime.conf is somewhat special,"
	einfo "as it supports variables which might allow you to keep a single"
	einfo "configuration for all your servers."
	einfo
	einfo "   # Edit config files"
	einfo "   cd /var/sublime/NAME"
	einfo "   vim /etc/sublime/sublime.conf server.properties *.yml"
	einfo
	einfo "   # Start the server"
	einfo "   rc-service sublime.NAME start"
	einfo
	einfo "All server instances are run inside a tmux console for easy access."
	einfo "To open the console of a running server instance, execute:"
	einfo
	einfo "    rc-service sublime.NAME console"
	einfo
	einfo "Thats just it. (*-*)"
}
