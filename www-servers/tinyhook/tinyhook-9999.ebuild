# Copyright 1999-2017 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=6

inherit git-r3 cmake-utils user

DESCRIPTION="A minimalistic webhook daemon for json requests"
HOMEPAGE="https://gitlab.com/nyronium/tinyhook"
EGIT_REPO_URI="https://gitlab.com/nyronium/tinyhook"

if [[ $PV == 9999 ]]; then
	KEYWORDS=""
else
	EGIT_COMMIT="v${PV}"
	KEYWORDS="~amd64 ~arm ~arm64 ~x86"
fi

LICENSE="MIT"
SLOT="0"
IUSE="+test"

DEPEND="
	test? ( dev-cpp/gtest )
	>=dev-cpp/npoll-1.2.1
	>=dev-cpp/nopts-1.2.1
	>=dev-cpp/nsys-0.9.3
	>=dev-cpp/nlohmann-json-3.3.0
	>=dev-libs/libfmt-5.1.0
	>=dev-libs/spdlog-1.2.1
	>=dev-cpp/ms-gsl-2.0.0"
RDEPEND="${DEPEND}"

pkg_setup() {
	enewuser tinyhook -1 -1 /var/lib/tinyhook
}

src_configure() {
	local mycmakeargs=(
		-DTINYHOOK_TESTS=$(usex test)
	)

	cmake-utils_src_configure
}

src_install() {
	cmake-utils_src_install

	# Install init.d and conf.d scripts
	newinitd "${FILESDIR}"/init tinyhook
	newconfd "${FILESDIR}"/conf tinyhook

	# Install /etc/tinyhook.conf
	insinto /etc
	doins support/tinyhook.conf
	fowners root:tinyhook /etc/tinyhook.conf
	fperms 640 /etc/tinyhook.conf

	# Install fix-permissions.sh script
	exeinto /usr/share/tinyhook
	doexe support/fix-permissions.sh
}
