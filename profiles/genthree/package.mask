# ban systemd and udev
sys-apps/systemd
sys-fs/udev

# don't use gperf 3.1 because media-gfx/feh won't compile
#=dev-util/gperf-3.1

# mask dev-libs/icu-59.1 due to compilation errors when building dev-libs/beecrypt
=dev-libs/icu-59.1

# mask openrc-0.30 because start-stop-daemon has a serious bug preventing many services from being stopped.
=sys-apps/openrc-0.30
