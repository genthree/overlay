# Copyright 1999-2017 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=6

inherit vcs-snapshot

DESCRIPTION="Manages the /usr/bin/gnvim symlink"
HOMEPAGE="https://gitlab.com/genthree/eselect-gnvim"
SRC_URI="https://gitlab.com/genthree/${PN}/repository/archive.tar.bz2?ref=v${PV} -> ${P}.tar.bz2"

LICENSE="GPL-2"
SLOT="0"
KEYWORDS="alpha amd64 arm arm64 hppa ia64 ~m68k ~mips ppc ppc64 ~s390 ~sh sparc x86 ~ppc-aix ~x64-cygwin ~amd64-fbsd ~sparc-fbsd ~x86-fbsd ~amd64-linux ~x86-linux ~ppc-macos ~x64-macos ~x86-macos ~m68k-mint ~sparc-solaris ~sparc64-solaris ~x64-solaris ~x86-solaris"
IUSE=""

DEPEND=""
RDEPEND=">=app-admin/eselect-1.0.6"

src_install() {
	insinto /usr/share/eselect/modules
	doins gnvim.eselect
}

pkg_postinst() {
	eselect gnvim update --if-unset
}

pkg_prerm() {
	[[ -z $REPLACED_BY_VERSION ]] && eselect gnvim unset
}
