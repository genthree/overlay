# Copyright 1999-2017 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=6
inherit toolchain-funcs bash-completion-r1

DESCRIPTION="A manual tiling window manager for X"
HOMEPAGE="http://herbstluftwm.org"
SRC_URI="http://herbstluftwm.org/tarballs/${P}.tar.gz"

LICENSE="BSD-2"
SLOT="0"
KEYWORDS="amd64 x86"
IUSE="examples xinerama zsh-completion"

CDEPEND=">=dev-libs/glib-2.24:2
	x11-libs/libX11
	x11-libs/libXext
	xinerama? ( x11-libs/libXinerama )"
DEPEND="${CDEPEND}
	virtual/pkgconfig"
RDEPEND="${CDEPEND}
	app-shells/bash
	x11-apps/xsetroot
	x11-misc/dzen
	zsh-completion? ( app-shells/zsh )"

src_compile() {
	emake CC="$(tc-getCC)" LD="$(tc-getCC)" COLOR=0 VERBOSE= \
		$(use xinerama || echo XINERAMAFLAGS= XINERAMALIBS= )
}

src_install() {
	# binaries and man pages
	dobin herbstluftwm herbstclient "${FILESDIR}"/herbstpanel
	doman doc/{herbstluftwm,herbstclient}.1

	# docs
	dodoc BUGS MIGRATION NEWS README.md

	# config
	exeinto /etc/xdg/herbstluftwm
	doexe "${FILESDIR}"/autostart

	insinto /etc/herbstluftwm
	doins -r "${FILESDIR}"/autostart.d

	# default theme
	exeinto /usr/share/herbstluftwm/themes
	doexe "${FILESDIR}"/themes/default

	# default panel
	exeinto /etc/xdg/herbstluftwm
	doexe share/panel.sh
	exeinto /usr/share/herbstluftwm/panels
	doexe "${FILESDIR}"/panels/default

	# eselect modules
	insinto /usr/share/eselect/modules
	doins "${FILESDIR}"/herbstluftwm-theme.eselect
	doins "${FILESDIR}"/herbstluftwm-panel.eselect

	# xsessions file
	insinto /usr/share/xsessions
	doins share/herbstluftwm.desktop

	# bash completion
	newbashcomp share/herbstclient-completion herbstclient

	# zsh completion
	if use zsh-completion ; then
		insinto /usr/share/zsh/site-functions
		doins share/_herbstclient
	fi

	# examples
	if use examples ; then
		exeinto /usr/share/doc/${PF}/examples
		doexe scripts/*.sh
		docinto examples
		dodoc scripts/README
		docompress -x /usr/share/doc/${PF}/examples
	fi
}

pkg_postinst() {
	eselect herbstluftwm-theme update --if-unset
	eselect herbstluftwm-panel update --if-unset
}

pkg_prerm() {
	if [[ -z $REPLACED_BY_VERSION ]] ; then
		eselect herbstluftwm-theme unset
		eselect herbstluftwm-panel unset
	fi
}
