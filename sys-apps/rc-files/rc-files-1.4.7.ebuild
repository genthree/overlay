# Copyright 1999-2017 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=6

inherit vcs-snapshot

DESCRIPTION="Provides several system configuration files"
HOMEPAGE="https://gitlab.com/genthree/rc-files"
SRC_URI="https://gitlab.com/genthree/${PN}/repository/archive.tar.bz2?ref=v${PV} -> ${P}.tar.bz2"

LICENSE="MIT"
SLOT="0"
KEYWORDS="~alpha ~amd64 ~arm ~arm64 ~hppa ~ia64 ~mips ~ppc ~ppc64 ~s390 ~sh ~sparc ~x86"
IUSE="X acpi +audit +bash +dracut +eix +fcron +git +login +logrotate +lynis +portage +readline server +ssh +sysctl +syslog-ng +sysstat +tmux +vim +neovim"

# require at least one flag
REQUIRED_USE="|| ( ${IUSE//\+/} )"

DEPEND=""
RDEPEND=""

src_install() {
	dodir /etc
	dodir /etc/skel

	if use X ; then
		insinto /etc
		doins -r Xresources

		insinto /etc/skel
		doins skel/.Xresources
		doins skel/.xinitrc
		fperms 0600 /etc/skel/.Xresources
		fperms 0600 /etc/skel/.xinitrc
	fi

	if use acpi ; then
		insinto /etc
		doins -r acpi
		fperms 0755 /etc/acpi/genthree.sh
	fi

	if use audit ; then
		insinto /etc
		doins -r audit
	fi

	if use bash ; then
		insinto /etc
		doins -r bash
		doins -r profile.d

		insinto /etc/skel
		doins skel/.bash_profile
		doins skel/.bashrc
		fperms 0600 /etc/skel/.bash_profile
		fperms 0600 /etc/skel/.bashrc
	fi

	if use dracut ; then
		insinto /etc
		doins -r dracut.conf.d
	fi

	if use fcron ; then
		insinto /etc
		doins -r fcron
		fowners -R root:fcron /etc/fcron
		fperms -R 640 /etc/fcron
		fperms 750 /etc/fcron
	fi

	if use eix ; then
		insinto /etc
		doins -r eixrc
	fi

	if use git ; then
		insinto /etc
		doins gitconfig
	fi

	if use login ; then
		insinto /etc
		doins login.defs
		doins issue issue.logo
	fi

	if use logrotate ; then
		insinto /etc
		doins -r logrotate.d
		doins logrotate.conf
	fi

	if use lynis ; then
		insinto /etc
		doins -r lynis

		insinto /etc/cron.daily
		doins cron.daily/lynis
	fi

	if use portage ; then
		insinto /etc
		doins -r portage
	fi

	if use readline ; then
		insinto /etc
		doins inputrc
	fi

	if use ssh ; then
		insinto /etc
		doins -r ssh
		fperms 0600 /etc/ssh/sshd_config
	fi

	if use sysctl ; then
		insinto /etc
		doins sysctl.conf

		insinto /etc/sysctl.d
		doins sysctl.d/20-genthree.conf

		if use server ; then
			doins sysctl.d/30-genthree-server.conf
		fi
	fi

	if use syslog-ng ; then
		insinto /etc
		doins -r syslog-ng
	fi

	if use sysstat ; then
		insinto /etc
		doins sysstat
	fi

	if use tmux ; then
		insinto /etc
		doins tmux.conf
	fi

	if use vim ; then
		insinto /etc/vim
		doins vim/{g,}vimrc.local
	fi

	if use neovim ; then
		insinto /etc/vim
		doins vim/nvimrc.local
	fi
}

pkg_postinst() {
	if use acpi && [[ -e /etc/acpi/default.sh ]] && [[ -e /etc/acpi/events/default ]]; then
		ewarn "The default acpi handler script /etc/acpi/default.sh"
		ewarn "might interfere with the genthree handler script."
		ewarn ""
		ewarn "Consider deleting /etc/acpi/events/default"
		ewarn "to stop it from being invoked."
	fi
}
