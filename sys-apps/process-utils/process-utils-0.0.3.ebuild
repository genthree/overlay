# Copyright 1999-2017 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=6

inherit vcs-snapshot

DESCRIPTION="Various useful utility programs"
HOMEPAGE="https://gitlab.com/nyronium/process-utils"
SRC_URI="https://gitlab.com/nyronium/${PN}/repository/archive.tar.bz2?ref=v${PV} -> ${P}.tar.bz2"

LICENSE="MIT"
SLOT="0"
KEYWORDS="~amd64 ~x86"
IUSE=""

DEPEND=""
RDEPEND=""

src_compile() {
	emake || die "Could not compile process-utils"
}

src_install() {
	dobin bin/*
}
