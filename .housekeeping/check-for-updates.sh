#!/bin/bash
#
# Housekeeping, upstream version checker
# and updater for the genthree overlay

GIT_COMMIT_OPTION="ci"


# Echo a status message $*
status() {
	echo "[1;34m$*:[m"
}

# Echo $* and exit 1
die() {
	echo " [1;31m* ERROR:[m $*" >&2
	exit 1
}

# die and clean_up
die_cleanup() {
	echo " [1;31m* ERROR:[m $*" >&2
	clean_up
	exit 1
}

# Prompt user for message ($*) (yes/no), yes is the default for no input
# Returns 0 for yes, 1 for no
prompt() {
	echo -n "$* ([1;32mYes[m/[1;31mno[m) " >&2

	# prompt until input is valid
	while true
	do
		local input=""
		read -r input

		case "${input,,}" in
			''|'yes')
				return 0
				;;

			'no')
				return 1
				;;

			*)
				echo -n "Sorry, response '$input' not understood. ([1;32mYes[m/[1;31mno[m) "
				;;
		esac
	done
}

declare -A IMPORT_VARIABLES
declare -A IMPORT_PREFIX_VARIABLES

# Registers special import variables ($*) for package ($1)
register_import_variables() {
	KEY=$1
	shift
	IMPORT_VARIABLES[$KEY]="$*"
}

# Registers special prefix (wildcard) import variables ($*) for package ($1)
register_import_prefix_variables() {
	KEY=$1
	shift
	IMPORT_PREFIX_VARIABLES[$KEY]="$*"
}

# Returns true (0) if the given package ($1) has extra import variables
has_import_variables() {
	[[ -n ${IMPORT_VARIABLES[$1]} ]]
}

# Returns true (0) if the given package ($1) has extra prefix import variables
has_import_prefix_variables() {
	[[ -n ${IMPORT_PREFIX_VARIABLES[$1]} ]]
}

# Gets all variables to import for given package ($1)
get_import_variables() {
	echo SRC_URI EGIT_REPO_URI EGIT_COMMIT_DATE

	if has_import_variables "$1" ; then
		echo ${IMPORT_VARIABLES[$1]}
	fi

	if has_import_prefix_variables "$1" ; then
		for i in ${IMPORT_PREFIX_VARIABLES[$1]} ; do
			echo $(eval "echo \${!$i*}")
		done
	fi
}

# Resets all imported variables
reset_imported_variables() {
	unset SRC_URI EGIT_REPO_URI EGIT_COMMIT_DATE

	if has_import_variables "$1" ; then
		unset ${IMPORT_VARIABLES[$1]}
	fi

	if has_import_prefix_variables "$1" ; then
		for i in ${IMPORT_PREFIX_VARIABLES[$1]} ; do
			for v in $(eval "echo \${!$i*}") ; do
				unset $v
			done
		done
	fi
}

# Imports all necessary variables from $EBUILD
import_ebuild() {
	parse_and_echo_ebuild_variables() {
		# Declare dummy inherit method
		inherit() { true; }

		# Sets all given parameters to their 'bash-identity', given "A" it calls
		#    eval A='${A}'
		set_identity() {
			for i in $*; do
				eval $i="'\${$i}'"
			done
		}

		# Echos all given variables so that evaling the output
		# results in the same vairable names and values
		export_variables() {
			for i in $*; do
				eval 'echo $i=${'$i'@Q}'
			done
		}

		# Expand P PVR PF
		P='${PN}-${PV}'
		PVR='${PV}-${PR}'
		PF='${PN}-${PV}-${PR}'

		# Set all common ebuild variables to identity
		set_identity \
			PN PV PR CATEGORY WORKDIR FILESDIR WORKDIR FILESDIR EBUILD_PHASE \
			EBUILD_PHASE_FUNC EPREFIX S T D ED MERGE_TYPE PROVIDES_EXCLUDE PORTAGE_LOG_FILE \
			PORTAGE_SOCKS5_PROXY REPLACED_BY_VERSION REPLACING_VERSIONS REQUIRES_EXCLUDE \
			ROOT EROOT DESCRIPTION EAPI SRC_URI HOMEPAGE KEYWORDS SLOT LICENSE IUSE DEPEND \
			RDEPEND HDEPEND PDEPEND REQUIRED_USE RESTRICT PROPERTIES PROVIDE DOCS \
			|| return 1

		# Source ebuild
		source "$EBUILD" &>/dev/null \
			|| return 1

		# Export variables
		export_variables \
			$(get_import_variables "$PACKAGE") \
			|| return 1

		return 0
	}

	# Do the parsing in a subshell and export needed variables
	# to prevent environment pollution
	local VARIABLES=$(parse_and_echo_ebuild_variables) \
		|| return 1

	eval "$VARIABLES"
}

# Find all packages containing ebuilds
find_packages() {
	local path
	for path in **/**/ ; do
		local CATEGORY="${path%%/*}"
		local PN="$(basename $path)"

		# Check for .ebuild existence
		local f
		for f in $path/$PN-*.ebuild ; do
			# Continue outer loop if there are no ebuilds
			[[ ! -e $f ]] && continue 2
			break
		done

		echo "$CATEGORY/$PN"
	done
}

# Get the length of the longest package name in $PACKAGES
get_longest_package_name_length() {
	local MAX_LENGTH=0
	local package
	for package in "${PACKAGES[@]}" ; do
		if [[ ${#package} -gt $MAX_LENGTH ]] ; then
			MAX_LENGTH=${#package}
		fi
	done

	echo $MAX_LENGTH
}

# prints $PACKAGE with at least $LONGEST_PACKAGE_NAME_LENGTH characters, does not print newline
print_package() {
	printf "  [32m%-${LONGEST_PACKAGE_NAME_LENGTH}s[m  :  " "$PACKAGE"
}

# Set global package related variables from package name given by $PACKAGE
global_set_package_category_and_pn_from_package() {
	CATEGORY="${PACKAGE%%/*}"
	PN=$(basename "$PACKAGE")
}

# Set global package related variables from ebuild given by $1
global_set_package_variables_from_ebuild() {
	local basename=$(basename "$EBUILD")

	PF=${basename%'.ebuild'}
	PVR=${PF#$PN-}
	PR=${PVR##*-}
	PV=${PVR%-$PR} PV=${PVR##*-}
}

# Find all ebuilds for given package
find_ebuilds() {
	find "$PACKAGE" -type f -name "$PN-*.ebuild" | sort -rV
}

# Calls lambda ($3) in a shallow (since $2) git clone of repo $1
git_remote() {
	local tmp="$(mktemp -d /tmp/git-log-remote.XXXXXX)" || return 1
	pushd "$tmp" &>/dev/null || return 1
	{ git clone --shallow-since="$2" -n "$1" . &>/dev/null && "$3"; } || {
			popd &>/dev/null;
			return 1;
		}
	popd &>/dev/null || return 1
	rm -rf "$tmp"
}

# Read git tags matching the tag regex ($2) from git repository ($1) and
# extract the '${PV}' part from the newest version tag found
git_query_remote_tag() {
	# Construct a regex which matches "refs/tags/$2"
	# but with ${PV} replaced by anything (\S\+)
	local tag_regex="refs/tags/${2/'${PV}'/'\S\+'}"

	# Extract the part before and after '${PV}'
	# to easily remove these later
	local tag_before_pv=${2%%'${PV}'*}
	local tag_after_pv=${2#*'${PV}'}

	# Query newest version tag on the remote repository
	local newest_tag=$(
		git ls-remote --tags $1 2>/dev/null \
			| grep -o "$tag_regex" \
			| sort -rV \
			| head -1
	) || return 1

	# remove the leading 'refs/tags/' part and the trailing '^{}' if necessary
	newest_tag=${newest_tag#'refs/tags/'}
	newest_tag=${newest_tag%'^{}'}

	# Extract the '${PV}' part from this tag
	local extracted_pv=${newest_tag%${tag_after_pv}}
	extracted_pv=${extracted_pv#${tag_before_pv}}

	# Echo extracted '${PV}' part
	echo $extracted_pv
}

# Query version from http page ($1) matching the tag regex ($2) and
# extract the '${PV}' part from the newest version found
query_http_version() {
	# Construct a regex which matches "refs/tags/$2"
	# but with ${PV} replaced by anything ([^"]\+)
	local tag_regex="${2/'${PV}'/'[^"]\+'}"

	# Extract the part before and after '${PV}'
	# to easily remove these later
	local tag_before_pv=${2%%'${PV}'*}
	local tag_after_pv=${2#*'${PV}'}

	# Query newest version by tag
	local newest_tag=$(curl -L -s -o - "$1" | grep -o "$tag_regex" | sort -rV | head -1) || return 1

	# Extract the '${PV}' part from this tag
	local extracted_pv=${newest_tag%${tag_after_pv}}
	extracted_pv=${extracted_pv#${tag_before_pv}}

	# Echo extracted '${PV}' part
	echo $extracted_pv
}

declare -A FIND_HOOKS

# Registers special find function ($2) for package ($1)
register_find_hook() {
	FIND_HOOKS[$1]=$2
}

# Returns true (0) if the given package ($1) has a find hook
has_find_hook() {
	[[ -n ${FIND_HOOKS[$1]} ]]
}

# Executes the find hook for given package ($1)
find_special() {
	"${FIND_HOOKS[$1]}"
}

# Fetches newest remote version from egit variables
# Returns values:
#   0 : success
#   1 : error (see $ERRNO)
#   2 : skip ebuild
find_newest_remote_version_egit() {
	PV_REMOTE_TYPE=egit

	# Check for 9999 ebuild, skip it if there are other ebuilds pending, stop if not
	[[ $PV == 9999 ]] && {
		[[ ${#EBUILDS[@]} == 1 ]] && {
			# If it is the only ebuild, stop here and report no new version.
			PV_REMOTE=$PV
			return 0
		}

		# Indicate to skip this ebuild
		return 2
	}

	# Check for valid EGIT_COMMIT_DATE
	[[ -z $EGIT_COMMIT_DATE ]] && {
		ERRNO="unknown use of egit"
		return 1
	}

	[[ $EGIT_COMMIT_DATE == '${PV}' ]] \
		&& EGIT_COMMIT_DATE="${PV}"

	# Check commits on remote repository
	local commits_since_local date_of_last_commit
	lambda() {
		commits_since_local="$(git rev-list --since="$EGIT_COMMIT_DATE" --count master)"
		date_of_last_commit="$(git log -1 --format=%cd --date=format:%Y.%m.%d)"
	}; git_remote "$EGIT_REPO_URI" "$EGIT_COMMIT_DATE" lambda || {
		ERRNO="git remote query failed"
		return 1
	}

	if [[ $commits_since_local -gt 0 ]] ; then
		PV_REMOTE=$date_of_last_commit
		PV_REMOTE_HINT="+$commits_since_local commits"
	else
		PV_REMOTE=$PV
	fi

	unset commits_since_local date_of_last_commit
}

# Fetches newest remote version from all EGIT_REPO_URI_* variables
# Returns values:
#   0 : success
#   1 : error (see $ERRNO)
#   2 : skip ebuild
find_newest_remote_version_egit_all() {
	for i in ${!EGIT_REPO_URI_*} ; do
		EGIT_REPO_URI=${!i}

		# Forward to normal egit find
		find_newest_remote_version_egit
	done
}

# Fetches newest remote version from github link given by $URI
# Returns values:
#   0 : success
#   1 : error (see $ERRNO)
#   2 : skip ebuild
find_newest_remote_version_fetch_github() {
	PV_REMOTE_TYPE=fetch

	# URI is a github archive link
	local git_repository=${URI%%/archive*}

	# Extract tag name
	local tag_regex=${URI##*archive/}
	tag_regex=${tag_regex%%.tar*}

	PV_REMOTE=$(git_query_remote_tag "$git_repository" "$tag_regex") || {
		ERRNO="github remote query failed"
		return 1
	}

	return 0
}

# Fetches newest remote version from gitlab link given by $URI
# Returns values:
#   0 : success
#   1 : error (see $ERRNO)
#   2 : skip ebuild
find_newest_remote_version_fetch_gitlab() {
	PV_REMOTE_TYPE=fetch

	# URI is a github archive link
	local git_repository=${URI%%/repository*}

	# Extract tag name
	local tag_regex=${URI##*\?ref=}
	tag_regex=${tag_regex%%.tar*}

	PV_REMOTE=$(git_query_remote_tag "$git_repository" "$tag_regex") || {
		ERRNO="gitlab remote query failed"
		return 1
	}

	return 0
}

# Fetches newest remote version from http link given by $URI
# Returns values:
#   0 : success
#   1 : error (see $ERRNO)
#   2 : skip ebuild
find_newest_remote_version_fetch_http() {
	PV_REMOTE_TYPE=fetch

	# URI might be a simple http directory, strip everything after last '/'
	local http_directory="${URI%/*}/"

	# Extract tag name from unexpanded URI
	local tag_regex=${URI##*/}
	tag_regex="${tag_regex%%.tar*}.tar"

	PV_REMOTE=$(query_http_version "$http_directory" "$tag_regex") || {
		ERRNO="http listing failed"
		return 1
	}

	[[ -z $PV_REMOTE ]] && {
		ERRNO="http listing returned crap"
		return 1
	}

	return 0
}

# Fetches newest remote version from http link given by $URI
# Returns values:
#   0 : success
#   1 : error (see $ERRNO)
#   2 : skip ebuild
find_newest_remote_version_fetch_http_teamspeak_client() {
	PV_REMOTE_TYPE=fetch

	local http_directory='https://www.teamspeak.com/en/downloads'
	local tag_regex='dl.4players.de/ts/releases/${PV}/TeamSpeak3-Client-linux_amd64'

	PV_REMOTE=$(query_http_version "$http_directory" "$tag_regex") || {
		ERRNO="http listing failed"
		return 1
	}

	[[ -z $PV_REMOTE ]] && {
		ERRNO="http listing returned crap"
		return 1
	}

	return 0
}

# Fetches newest remote version from http link given by $URI
# Returns values:
#   0 : success
#   1 : error (see $ERRNO)
#   2 : skip ebuild
find_newest_remote_version_fetch_http_teamspeak_server() {
	PV_REMOTE_TYPE=fetch

	local http_directory='https://www.teamspeak.com/en/downloads'
	local tag_regex='dl.4players.de/ts/releases/${PV}/teamspeak3-server_linux_amd64'

	PV_REMOTE=$(query_http_version "$http_directory" "$tag_regex") || {
		ERRNO="http listing failed"
		return 1
	}

	[[ -z $PV_REMOTE ]] && {
		ERRNO="http listing returned crap"
		return 1
	}

	return 0
}

# Find the newest remote version of the current package,
# sets the global variables $PV_REMOTE $PV_REMOTE_HINT $PV_REMOTE_TYPE
# Returns values:
#   0 : success
#   1 : error (see $ERRNO)
#   2 : skip ebuild
find_newest_remote_version() {
	# Clear variables
	unset ERRNO URI PV_REMOTE PV_REMOTE_HINT PV_REMOTE_TYPE

	local u
	for u in $SRC_URI ; do
		# Find URI starting with http
		[[ $u =~ ^http ]] || continue

		# Find URI containing ${PV} literally
		[[ $u =~ '${PV}' ]] || continue

		# Set URI
		URI=$u

		# Expand '${PN}' in $URI
		URI=${URI//'${PN}'/${PN}}
		break
	done

	# Process special packages independently
	if has_find_hook "$PACKAGE" ; then
		find_special "$PACKAGE"
	# Try automatic version finding otherwise
	elif [[ -z $URI ]] ; then
		# URI does not contain a link with '${PV}'
		# Check if the ebuild is EGIT based

		[[ -z $EGIT_REPO_URI ]] && {
			# The ebuild is not EGIT based. Stop here.
			ERRNO="unknown package source"
			return 1
		}

		find_newest_remote_version_egit
	else
		# URI is a http link that contains '${PV}'
		# Depending on $URI call the correct find function
		local _S="[^[:blank:]]"
		if [[ $URI =~ ://github\.$_S*/archive/ ]] ; then
			find_newest_remote_version_fetch_github
		elif [[ $URI =~ ://gitlab\.$_S*/repository/archive\. ]] ; then
			find_newest_remote_version_fetch_gitlab
		else
			find_newest_remote_version_fetch_http
		fi
	fi
}

UPDATE_FULL_EBUILD_PATH=()
UPDATE_PV_REMOTE=()
UPDATE_PV_REMOTE_HINT=()
UPDATE_PV_REMOTE_TYPE=()

# Register available update for later use
register_update() {
	UPDATE_FULL_EBUILD_PATH+=( "$EBUILD" )
	UPDATE_PV_REMOTE+=( "$PV_REMOTE" )
	UPDATE_PV_REMOTE_HINT+=( "$PV_REMOTE_HINT" )
	UPDATE_PV_REMOTE_TYPE+=( "$PV_REMOTE_TYPE" )
}

# Call "$1" for each update, with saved variables loaded
for_each_registered_update() {
	for i in "${!UPDATE_FULL_EBUILD_PATH[@]}" ; do
		EBUILD=${UPDATE_FULL_EBUILD_PATH[i]}
		PV_REMOTE=${UPDATE_PV_REMOTE[i]}
		PV_REMOTE_HINT=${UPDATE_PV_REMOTE_HINT[i]}
		PV_REMOTE_TYPE=${UPDATE_PV_REMOTE_TYPE[i]}
		PACKAGE=$(dirname "$EBUILD")

		global_set_package_category_and_pn_from_package
		global_set_package_variables_from_ebuild

		# Call lambda function
		"$1"
	done
}

# Get count of registered updates
get_registered_update_count() {
	echo "${#UPDATE_FULL_EBUILD_PATH[@]}"
}




# Check working directory
if [[ ! -d .git || ! -f metadata/layout.conf ]] ; then
	die "Must be executed from overlay's root directory"
fi


# Register find hooks for packages that need special handling
#register_import_prefix_variables 'dev-util/spirv-tools' EGIT_REPO_URI_
#register_find_hook 'dev-util/spirv-tools' find_newest_remote_version_egit_all

register_find_hook 'media-sound/teamspeak-client' find_newest_remote_version_fetch_http_teamspeak_client
register_find_hook 'media-sound/teamspeak-server' find_newest_remote_version_fetch_http_teamspeak_server


PACKAGES=( $(find_packages) )
LONGEST_PACKAGE_NAME_LENGTH=$(get_longest_package_name_length)

status "Checking for new versions"

for PACKAGE in "${PACKAGES[@]}" ; do
	# Set CATEGORY and PN
	global_set_package_category_and_pn_from_package

	# Print package
	print_package

	# Find all ebuilds sorted by version
	EBUILDS=( $(find_ebuilds "$PACKAGE") )
	for EBUILD in "${EBUILDS[@]}" ; do
		SKIPPED=false

		# Set remaining package variables
		global_set_package_variables_from_ebuild

		# Import variables from ebuild
		import_ebuild

		# Find the newest remote version of the package
		find_newest_remote_version
		STATUS=$?

		# Reset imported variables
		reset_imported_variables "$PACKAGE"

		# Switch status
		case "$STATUS" in
			0)
				# Print message and register_update if package is not up to date
				if [[ $PV == "$PV_REMOTE" ]] ; then
					echo "[1;32mUp to date   [m:  [1;33m($PV)[m"
				else
					register_update
					echo "[1;34mNew version  [m:  [1;33m($PV)[m -> [1;36m($PV_REMOTE${PV_REMOTE_HINT:+, }${PV_REMOTE_HINT})[m"
				fi

				break
				;;

			2)
				# Skip this ebuild and try with the next one
				SKIPPED=true
				continue
				;;

			*)
				# Could not find a new version because of internal errors.
				# Skip the package.
				SKIPPED=true
				break
				;;
		esac
	done

	# All ebuilds were skipped, mark the package as skipped.
	[[ $SKIPPED == true ]] && {
		echo "Skipped      [m:  [m$ERRNO[m"
	}
done


echo
if [[ $(get_registered_update_count) == 0 ]] ; then
	echo "[1;32mAll packages are up to date.[m"
	exit 0
else
	status "Summary of new versions"
fi

lambda() {
	print_package
	echo "[1;33m($PV)[m -> [1;36m(${PV_REMOTE}${PV_REMOTE_HINT:+, }${PV_REMOTE_HINT})[m"
}; for_each_registered_update lambda


echo
prompt "Automatically update ebuilds and commit changes?" \
	|| exit 0


# Trap SIGINT to clean up
clean_up() {
	echo "Cleaning up:[m" >&2
	echo "  Removing created ebuilds[m" >&2

	lambda() {
		NEW_EBUILD="$PACKAGE/$PN-$PV_REMOTE.ebuild"
		rm "$NEW_EBUILD" &>/dev/null || true
	}; for_each_registered_update lambda

	if [[ -n $BRANCH ]] ; then
		echo "  Checking out $BRANCH[m" >&2
		# get back to saved $BRANCH
		git checkout --quiet "$BRANCH"
	fi

	[[ $STASHED == true ]] && {
		echo "  Restoring stash[m" >&2
		git stash pop --quiet || \
			die "Could not pop stash"
	}
}

# Trap SIGINT to clean up
trap_SIGINT() {
	echo >&2
	echo "[1;31mCatched Ctrl-C" >&2
	clean_up
	echo "[m" >&2

	exit 130
}

trap trap_SIGINT SIGINT


echo
declare -A SKIP_PACKAGES
if prompt "Select specific packages to update?" ; then
	lambda() {
		print_package
		prompt "Select?" \
			|| SKIP_PACKAGES["$PACKAGE"]=true
	}; for_each_registered_update lambda
fi


# Check for staged changes
if ! git diff-index --cached --quiet HEAD -- ; then
	echo
	echo "[1;33mWARNING: You have staged changes![m"
	prompt "Intermediately stash changes and proceed?" \
		|| exit 0

	echo "[1;32mStashed changes[m"
	git stash --quiet || \
		die "Could not stash changes"

	STASHED=true
fi


echo
status "Creating ebuilds"

lambda() {
	[[ "${SKIP_PACKAGES[$PACKAGE]}" == true ]] \
		&& return 0

	print_package

	# Copy ebuild
	NEW_EBUILD="$PACKAGE/$PN-$PV_REMOTE.ebuild"
	cp -n "$EBUILD" "$NEW_EBUILD" || {
		echo "[1;31mcopying ebuild failed[m"
		return 1
	}

	echo "[1;36m$(basename "$NEW_EBUILD")[m"
}; for_each_registered_update lambda


echo
status "Queueing distfiles"

FETCH_QUEUE=""
lambda() {
	[[ "${SKIP_PACKAGES[$PACKAGE]}" == true ]] \
		&& return 0

	print_package

	# Queue ebuild.
	NEW_EBUILD="$PACKAGE/$PN-$PV_REMOTE.ebuild"
	FETCH_QUEUE="$FETCH_QUEUE $NEW_EBUILD"

	echo "[1;33mQueued[m"
}; for_each_registered_update lambda


if id -nG | grep -qw 'portage' ; then
	# User does belong to the portage group, so repoman
	# can fetch the new distfiles without superuser privileges
	FETCHED=false
else
	echo
	echo "User '$(id -un)' does not belong to the 'portage' group."
	echo "Requesting superuser privileges to fetch distfiles:"

	# Fetch new ebuild sources
	su_lambda() {
		for EBUILD in ${FETCH_QUEUE} ; do
			# Set package variables
			PACKAGE=$(dirname "$EBUILD")
			global_set_package_category_and_pn_from_package
			global_set_package_variables_from_ebuild

			# Print package
			print_package

			# Fetch file
			etmpbuild "$EBUILD" manifest &>/dev/null || {
				echo "[1;31metmpbuild fetch failed[m"
				die_cleanup "Cannot proceed because of previous failure"
			}

			echo "[1;32mFetch OK[m"
		done
	}

	export -f su_lambda
	export -f global_set_package_category_and_pn_from_package
	export -f global_set_package_variables_from_ebuild
	export -f print_package
	export LONGEST_PACKAGE_NAME_LENGTH
	export FETCH_QUEUE

	# Try three times maximum
	su -p -c su_lambda \
		|| su -p -c su_lambda \
		|| su -p -c su_lambda \
		|| die_cleanup "Could not aquire superuser privileges"

	FETCHED=true
fi


echo
[[ $FETCHED == true ]] \
	&& status "Creating manifests" \
	|| status "Fetching files and creating manifests"

lambda() {
	[[ "${SKIP_PACKAGES[$PACKAGE]}" == true ]] \
		&& return 0

	print_package

	# Create manifest file
	(
		cd "$PACKAGE"

		# repoman returns success even if there was an error.
		# Delete manifest to force repoman to create it,

		rm Manifest

		# Additionally check Manifest existence
		repoman manifest >/dev/null && \
			[[ -f Manifest ]]
	) || {
		[[ $FETCHED == true ]] \
			&& echo "[1;31mcreating manifest failed[m" \
			|| echo "[1;31mfetching files or creating manifest failed[m"
		die_cleanup "Cannot proceed because of previous failure"
	}

	[[ $FETCHED == true ]] \
		&& echo "[1;32mManifest OK[m" \
		|| echo "[1;32mFetch OK, Manifest OK[m"
}; for_each_registered_update lambda


echo
status "Committing changes"

# Save BRANCH for Ctrl-C
BRANCH=$(git symbolic-ref --short HEAD)

lambda() {
	[[ "${SKIP_PACKAGES[$PACKAGE]}" == true ]] \
		&& return 0

	print_package

	# Commit changes
	NEW_EBUILD="$PACKAGE/$PN-$PV_REMOTE.ebuild"
	MANIFEST="$PACKAGE/Manifest"
	git add "$NEW_EBUILD" "$MANIFEST" || {
		echo "[1;31mgit add failed[m"
		die_cleanup "Cannot proceed because of previous failure"
	}

	git $GIT_COMMIT_OPTION --quiet -m "${CATEGORY}/${PN}: automatic update to version $PV_REMOTE" || {
		echo "[1;31mgit commit failed[m"
		die_cleanup "Cannot proceed because of previous failure"
	}

	echo "[1;32mCommit OK[m"
}; for_each_registered_update lambda
echo


# Restore stash
if [[ $STASHED == true ]] ; then
	git stash pop --quiet || \
		die "Could not pop stash"
	echo "[1;32mStash restored[m"
fi


echo "[1;32mUpdate successful, please review all automatic commits before pushing.[m"
