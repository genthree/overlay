genthree
========

This repository provides ebuilds and profiles of various categories,
a short summary (completeness not guaranteed) is shown here.

Improvements and updates to existing packages:
--------

These packages have been improved:

* Improved configuration system (conf.d style) for `x11-wm/herbstluftwm` including eselect module for theme and panel
* Automatic firejailing for `media-sound/teamspeak-server-bin`
* Automatic downloads for `media-sound/teamspeak-server-bin` and `media-sound/teamspeak-client-bin`
* More patched powerline fonts `media-fonts/powerline-fonts`

New ebuilds
--------

New content is provided:

* Vulkan docs `app-doc/vulkan-docs` (man pages etc.)
* Neovim plugins (new category `app-nvim`)
	* deoplete `app-nvim/deoplete`
	* enhance-cpp-highlight `app-nvim/cpp-enhanced-highlight`
	* matter colorscheme `app-nvim/matter`
* Neovim gui(s)
	* neovim-qt `app-editors/neovim-qt`
	* eselect module `app-eselect/eselect-gnvim`
* Kernel configuration system (for better kernel config management and easier build process)
* Many system configuration files in `sys-apps/rc-files`
* Bash completion for tmux `app-shells/tmux-bash-completion`

Profiles
--------

This repository also provides a group of profiles `profiles/genthree/*`, which
aim to provide a reasonable default set of packages and options for different
systems, including content like:

* Enhanced security with `sys-apps/firejail` and `sys-admin/apparmor`
* A set of packages needed to administrate a system:
	* Portage tools like: `app-portage/eix`, `app-portage/gentoolkit`, `app-portage/repoman`, ...
	* A resonable editor: `app-editors/nvim`
	* Tools for network administration: `net-analyzer/iftop`, `net-analyzer/traceroute`, ...
* Better default configuration files for many packages `sys-apps/rc-files`
