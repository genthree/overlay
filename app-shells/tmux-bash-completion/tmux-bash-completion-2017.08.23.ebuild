# Copyright 1999-2017 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=6

inherit bash-completion-r1 git-r3

DESCRIPTION="bash-completion scripts for tmux"
HOMEPAGE="https://github.com/imomaliev/tmux-bash-completion"
SRC_URI=""
EGIT_REPO_URI="https://github.com/imomaliev/tmux-bash-completion.git"
EGIT_COMMIT_DATE="${PV}"

LICENSE="GPL-2"
SLOT="0"
KEYWORDS="~alpha ~amd64 ~arm ~arm64 ~hppa ~ia64 ~mips ~ppc ~ppc64 ~s390 ~sh ~sparc ~x86 ~amd64-fbsd ~sparc-fbsd ~x86-fbsd ~amd64-linux ~x86-linux ~ppc-macos ~x64-macos ~x86-macos ~m68k-mint ~sparc-solaris ~sparc64-solaris"
IUSE=""

DEPEND="
	>=app-misc/tmux-2.2
	app-shells/bash-completion
"
RDEPEND="${DEPEND}"

src_install() {
	default
	dobashcomp "${S}"/completions/tmux
}
